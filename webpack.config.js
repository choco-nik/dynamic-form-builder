const path = require('path');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');

module.exports = (_, argv) => {
  let isDevelopment = argv.mode !== 'production';
  let config = {
  entry: './src/application.js',
  mode: isDevelopment ? 'development' : 'production',
  resolve: {
    extensions: ['.js']
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: [isDevelopment && require.resolve('react-refresh/babel')].filter(Boolean)
        }
      },
      {test:/\.css$/,use:['style-loader','css-loader']},
    ]
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: '/public/',
    filename: 'application.js'
  },
  devServer: {
    static: {
      directory: path.join(__dirname, 'public')
    },
    port: 3000
  },
  plugins: [isDevelopment && new ReactRefreshWebpackPlugin()].filter(Boolean),
  }
  return config;
}