import React, { useState } from 'react';

const DynamicFormBuilder = ({ formJSON }) => {
  const [formData, setFormData] = useState({});

  const handleInputChange = (fieldId, value) => {
    setFormData((prevData) => ({
      ...prevData,
      [fieldId]: value,
    }));
  };

  const renderField = (field) => {
    switch (field.type) {
      case 'text':
        return (
          <div className='form-item-text'>
            <input
              type="text"
              id={field.id}
              placeholder={field.placeholder}
              value={formData[field.id] || ''}
              onChange={(e) => handleInputChange(field.id, e.target.value)}
            />
          </div>
        );
      case 'select':
        return (
          <div className='form-item-select'>
            <select
              id={field.id}
              value={formData[field.id] || ''}
              onChange={(e) => handleInputChange(field.id, e.target.value)}
            >
              <option value="">Select...</option>
              {field.options.map((option, index) => (
                <option key={index} value={option.label}>
                  {option.label}
                </option>
              ))}
            </select>
          </div>
        );
      case 'checkbox':
        return (
          <input
            type="checkbox"
            id={field.id}
            checked={formData[field.id] || false}
            onChange={(e) => handleInputChange(field.id, e.target.checked)}
          />
        );
      default:
        return null;
    }
  };

  const renderForm = () => {
    return formJSON.map((section, index) => (
      <div key={index}>
        {section.fields.map((field) => (
          <div key={field.id}>
            <label htmlFor={field.id}>{field.label}</label>
            {renderField(field)}
          </div>
        ))}
      </div>
    ));
  };

  return (
    <div className='container'>
      <h2>Dynamic Form Builder</h2>
      <form>
        {renderForm()}
        <button className='button' type="submit">
          Submit
        </button>
      </form>
    </div>
  );
};

export default DynamicFormBuilder;
