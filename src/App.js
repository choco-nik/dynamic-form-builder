import React from 'react';
import DynamicFormBuilder from './DynamicFormBuilder';

const App = () => {
  const formJSON = [
    {
      fields: [
        {
          id: "firstname", // text
          label: "First Name",
          required: true,
          placeholder: "Enter your first name ...",
          type: "text",
          value: ""
        },
        {
          id: "lastname", // text
          label: "Last Name",
          required: true,
          placeholder: "Enter your last name ...",
          type: "text",
          value: ""
        },
        {
          id: "country", // select
          label: "County",
          required: true,
          type: "select",
          options: [
            { label: "Bulgaria" },
            { label: "Greece" },
            { label: "Romania" },
            { label: "Serbia" }
          ],
          value: ""
        },
        {
          id: "subscribe", // checkbox
          label: "Subscribe to newsletter",
          type: "checkbox",
          value: ""
        }
      ]
    }
  ];

  return <DynamicFormBuilder formJSON={formJSON} />;
};

export default App;