import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import '../public/index.css';

const appContainer = document.getElementById('app');
const app = createRoot(appContainer);
app.render(<App />)